import torch
import torch.nn as nn
import torch.nn.functional as F


class CSPUpBlock(nn.Module):
    def __init__(self, in_channels):
        super(CSPUpBlock, self).__init__()
        self.upsample = nn.ConvTranspose2d(in_channels//2, in_channels//2, kernel_size=2, stride=2)
        self.upsample1 = nn.ConvTranspose2d(in_channels//2, in_channels//2, kernel_size=2, stride=2)
        self.conv1 = nn.Conv2d(in_channels, in_channels//2, kernel_size=3, padding=1)
        self.relu = nn.ReLU(inplace=True)
        self.conv3_1 = nn.Conv2d(in_channels//2, in_channels//2, kernel_size=3, padding=1)
        self.conv3_2 = nn.Conv2d(in_channels//2, in_channels//2, kernel_size=3, padding=1)

    def forward(self, x):
        # 1 way
        conv0_x = self.conv1(x)
        upsampled_x = self.upsample(conv0_x)

        # 2 way
        conv1_x = self.conv1(x)
        relu_x = self.relu(conv1_x)
        upsampled_conv1_x = self.upsample1(relu_x)
        conv3_x1 = self.conv3_1(upsampled_conv1_x)
        relu_conv3_x1 = self.relu(conv3_x1)
        conv3_x2 = self.conv3_2(relu_conv3_x1)

        # merge
        out = upsampled_x + conv3_x2

        return out


class Encoder(nn.Module):
    def __init__(self, in_channels):
        super(Encoder, self).__init__()
        self.csp_up_block1 = CSPUpBlock(in_channels=in_channels)
        self.csp_up_block2 = CSPUpBlock(in_channels=in_channels/2)
        self.csp_up_block3 = CSPUpBlock(in_channels=in_channels/4)
        self.csp_up_block4 = CSPUpBlock(in_channels=in_channels/8)

        self.deconv2d = nn.ConvTranspose2d(in_channels/16, 3, kernel_size=2, stride=2)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.csp_up_block1(x)
        x = self.csp_up_block2(x)
        x = self.csp_up_block3(x)
        x = self.csp_up_block4(x)
        x = self.deconv2d(x)
        y = self.relu(x)
        return y



if __name__ == '__main__':
    print("Encoder row")
    x = torch.randn(1, 1024, 4, 4)
    csp_up_block = CSPUpBlock(in_channels=1024)
    out = csp_up_block(x)

    print("Output shape:", out.shape)

    csp_up_block = CSPUpBlock(in_channels=512)
    out = csp_up_block(out)

    print("Output shape:", out.shape)

    csp_up_block = CSPUpBlock(in_channels=256)
    out = csp_up_block(out)

    print("Output shape:", out.shape)

    csp_up_block = CSPUpBlock(in_channels=128)
    out = csp_up_block(out)

    print("Output shape:", out.shape)

    deconv2d = nn.ConvTranspose2d(64, 3, kernel_size=2, stride=2)
    relu = nn.ReLU(inplace=True)

    out = deconv2d(out)
    out = relu(out)

    print("Output shape:", out.shape)

    print("Decoder row")
